"use strict";
var KTDatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#tblStudent');

		if (table.length === 0) return false;
		// begin first table
		table.DataTable({
			responsive: false,
      searching: false,
      paging: false,
      ordering: false,
      info: false
		});

	};


	return {

		//main function to initiate the module
		init: function() {
			initTable1();
			// initTable2();
		}
	};
}();

jQuery(document).ready(function() {
	KTDatatablesBasicBasic.init();
});
