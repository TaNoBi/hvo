import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { UserComponent } from './pages/admin/user/user.component';
import {StudentComponent} from './pages/student/student.component';

const routes: Routes = [
  { path: '', component: ProfileComponent },
  { path: 'class', component: StudentComponent },
  { path: 'admin', children: [
      { path: 'user', component: UserComponent },
      { path: 'profile', component: ProfileComponent }
    ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponent = [ HomeComponent, ProfileComponent ];
