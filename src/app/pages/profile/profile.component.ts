import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
declare var $: any;
const quantityOfDays = 30;
import * as DataTable from '../../../assets/plugins/custom/datatables/datatables.bundle.js';
declare var DataTable: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.css',
    '../../../assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.5'
    // '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css',
    // '../../../../node_modules/bootstrap-table/dist/bootstrap-table.min.css',
    // '../../../assets/plugins/datatables/css/semantic.css',
    // '../../../assets/plugins/datatables/css/dataTables.semanticui.min.css'
  ]
})

export class ProfileComponent implements OnInit {
  headers: object;
  body: object;
  // dtOptions: DataTables.Settings = {};

  renderHeader(): object {
    const days = 30;
    let result = [[{rowspan: 2, text: 'STT'}, {rowspan: 2, class: 'min-w-200px', text: 'Họ và tên'}, {colspan: quantityOfDays, text: 'Ngày'}, {rowspan: 2, text: 'ND', class: 'min-w-200px'}, {rowspan: 2, text: 'HK', class: 'min-w-200px'}]];
    let trs = [];
    for (let i = 1; i <= quantityOfDays; i++) {
      trs.push({text: i});
    }
    result.push(trs);
    return result;
  }

  renderBody(): object {
    let result = [];
    for (let i = 1; i <= 20; i++) {
      const days = this.renderTd();
      // @ts-ignore
      result.push([i, 'Tran Van A', ...days, '', '']);
    }
    return result;
  }

  renderTd(): object {
    let result = [];
    for (let i = 0; i < quantityOfDays; i++) result.push('');
    return result;
  }


  ngOnInit(): void {
    this.headers = this.renderHeader();
    this.body = this.renderBody();
  }

  ngAfterViewInit(): void {
    $('#tblStudent').DataTable({
      paging: false,
      searching: false,
      ordering: false,
      info: false,
      fixedColumns: {
        leftColumns: 0,
        rightColumns: 1
      }
    });
  }

}
