import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  userList: object;
  table: object;
  constructor() { }

  async getData(): Promise<void> {
    try {
      const resp = await axios({
        method: 'POST',
        url: 'http://hvo.vn:3000/postCollectUser',
        data: {
          username: 'giaovien',
          status: 1
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      const {data, status} = resp;
      const {result} = data;
      return result;
      // console.log(resp);
    } catch (error) {
      console.log(error);
    }
  }

  async ngOnInit(): Promise<void> {
    const data = await this.getData();
    console.log('ngOnInit', data);
    this.table = $('#tblUsers').DataTable({
      columns: [
        {data: 'username', title: 'Username'},
        {data: 'email', title: 'Email'},
        {data: 'status', title: 'Status'},
        {
          data: 'createdate',
          title: 'Created date',
          // tslint:disable-next-line:typedef
          render(text, type, row, meta ) {
            return text ? moment(text).format('DD/MM/YYYY HH:mm:ss'): '';
          }
        },
      ],
      data,
      paging: false,
      info: false,
      searching: false
    });
  }

  ngAfterViewInit(): void {

  }

  ngOnChanges(): void {

  }
}
