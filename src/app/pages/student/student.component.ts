import { Component, OnInit } from '@angular/core';
import axios from 'axios';
declare var $: any;

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  classList: object;
  table: object;
  constructor() { }

  async getClassList() {
    const classes = await axios({
      method: 'POST',
      url: 'http://hvo.vn:3000/postColectMobileClass',
      data: {
        username: '1212',
        tokenid: 2121
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
    const {data, status} = classes;
    const {result} = data;
    return result;
  }

  async getStudentByClass(classId): Promise<object> {
    const classes = await axios({
      method: 'POST',
      url: 'http://hvo.vn:3000/postGetMemberByClassID',
      data: {
        username: '21',
        tokenid: 2121,
        class_id: classId
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
    const {data, status} = classes;
    const {result} = data;
    return result;
  }

  async getData(): Promise<void> {
    try {
      const classes = await this.getClassList();
      for (let i = 0; i < classes.length; i++) {
        const item = classes[i];
        $('#cbxClasses').append('<option value="' + item.id + '">' + item.desc + '</option>');
      }
      // console.log(resp);
    } catch (error) {
      console.log(error);
    }
  }

  async ngOnInit(): Promise<void> {
    const data = await this.getStudentByClass(1);
    console.log('ngOnInit', data);
    this.table = $('#tblClasses').DataTable({
      columns: [
        {data: 'code', title: 'Mã'},
        {data: 'name', title: 'Họ tên'},
        // {data: 'type', title: 'Status'},
        {data: 'parent_data', title: 'Phụ huynh'},
      ],
      data,
      paging: false,
      info: false
    });
  }

}
